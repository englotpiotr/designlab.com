---
name: List
description: A list consists of related pieces of information grouped together, clearly associated to each other. Lists are easy to read and maintain, and they provide a good structural point of view for interface elements.
figma: https://www.figma.com/file/qEddyqCrI7kPSBjGmwkZzQ/Pajamas-UI-Kit?node-id=425%3A128
docs: in-progress
related:
  - navigation
  - card
  - tabs
  - breadcrumb
  - dropdown
  - sorting
---

Lists are frequently used for navigation, cards, tabs, breadcrumbs, dropdown content, and general text blocks.

## Usage

- A list must contain one or more list elements.
- Arrange items in a logical way. For example, in alphabetical or numeric order, or according to a specific use case. Lists can also sometimes be rearranged based on a users' preference if a [sort](/components/sorting) component is available.
- Use a list to group a continuous set of text and images. For example, a list of members of a project.

## Types

- **Unordered list**: The order of list items is not strict. List items are marked with plain bullets, which may be omitted in the UI. Uses the `<ul>` tag.
- **Ordered list**: The order of list items is strict. List items are marked with numbers. Uses the `<ol>` tag.
- **Definition list**: The order of list items matches that of a dictionary. Uses the `<dl>` tag.

### List layout types

List items can have single or multiple lines of text, with a maximum of three lines.

A list contains a single column of content, and each list item is place in an individual row.

Lists are commonly laid out vertically, but the items can be positioned horizontally depending on CSS styles. For example, see [Tabs](/components/tabs).

## Demo

Todo: Add component demo

## Design specifications

Color, spacing, dimension, and layout specific information pertaining to this component can be viewed using the following link:

[View design in Pajamas UI Kit →](https://www.figma.com/file/qEddyqCrI7kPSBjGmwkZzQ/Pajamas-UI-Kit-Beta?node-id=2864%3A4)
