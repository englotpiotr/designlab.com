---
name: Color picker
description: A color picker allows users to customize a component by choosing a color option. It is only available in instances where a user is adding bespoke content that needs to be differentiated.
docs: in-progress
related:
  - form
---

## Usage

The color picker comes in two parts: the input field and the color swatch preview. 

The input field is pre-populated with the default value of `#6699CC`. The color swatch preview allows a user to visualize what HEX code is currently inputted.

The user may edit the color by:
 
* Choosing a new color from the list of suggestions
* Using the color picker popover
* Entering a HEX value

When using the input field, users are able to only enter HEX formats. For example, the user can type `#000` or `#000000`.

Todo: Add live component block with code example (inputs of different widths)

## Design specification

Color, spacing, dimension, and layout specific information pertaining to this component can be viewed using the following link:

Todo: Add to Pajamas UI Kit

